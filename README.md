# Abdal Blowfish Encryption

## Project Programmer
> Ebrahim Shafiei (EbraSha) - Prof.Shafiei@Gmail.com

## Made For 

Abdal team's new tool called Abdal Blowfish Encryption can encrypt your texts with the Blowfish algorithm to protect your information in insecure spaces. This tool also supports Salt generation for PhpMyAdmin configuration

**Requires**
> Visual Studio 2019 - Telerik WinForm - Chilkat - .NetFramework 4.* - nSoftware
>


Features

- Encrypt Message
- Support Blowfish Encryption
- Beautiful appearance
- Has an installation package
- No malware
- Open Source
- Very high speed
- Free

## ❤️ Donation
> USDT:      TXLasexoQTjKMoWarikkfYRYWWXtbaVadB

> bitcoin:   19LroTSwWcEBY2XjvgP6X4d6ECZ17U2XsK

> For Iranian People -> MellatBank : 6104-3378-5301-4247



## Reporting Issues

If you are facing a configuration issue or something is not working as you expected to be, please use the **Abdal.Group@Gmail.Com** or **Prof.Shafiei@Gmail.com** . Issues on GitLab are also welcomed.
